clear
clc
client=genomix.client();
client.rpath('/home/fspindle/programming_ws/devel/lib/genom/pocolibs/plugins');
rotorcraft=client.load('rotorcraft');
pom=client.load('pom');
vicon=client.load('vicon');
result_vicon = vicon.connect('192.168.30.1:801');

connect=rotorcraft.connect('/dev/ttyUSB0', 57600)

result_pom_imu = pom.connect_port('measure/imu','rotorcraft/imu');
pom.add_measurement('imu');
result_pom_vicon = pom.connect_port('measure/mocap','vicon/bodies/mkQuadro0');
pom.add_measurement('mocap');

pom.log_state()