/*
 * Copyright (c) 2015-2020 LAAS/CNRS
 * All rights reserved.
 *
 * Redistribution and use  in source  and binary  forms,  with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of  source  code must retain the  above copyright
 *      notice and this list of conditions.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice and  this list of  conditions in the  documentation and/or
 *      other materials provided with the distribution.
 *
 *                                      Anthony Mallet on Mon Feb 16 2015
 */
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/time.h>

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>  //pour la structure pollfd
#include <stdarg.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>     //pour ecrire write function

#ifdef HAVE_LOW_LATENCY_IOCTL
# include <sys/ioctl.h>
# include <linux/serial.h> /* for TIOCGSERIAL and ASYNC_LOW_LATENCY */
#endif

#ifdef __linux__
# include <libudev.h>
#endif

#include "codels.h"


/* --- mk_open_tty --------------------------------------------------------- */

static const char *	usb_serial_to_tty(const char *serial);
static int		ftdi_sio_check_latency(int fd);

/* Open a serial port, configure to the given baud rate */
int
mk_open_tty(const char *device, uint32_t speed)
{
  const char *path;
  struct termios t;
  speed_t baud;
  int fd;

  /* select baud rate */
//#ifndef B9600
//# define B9600 9600U
//#endif
#ifndef B57600
# define B57600 57600U
#endif
#ifndef B115200
# define B115200 115200U
#endif
#ifndef B256000
# define B256000 256000U
#endif
#ifndef B500000
# define B500000 500000U
#endif
#ifndef B2000000
# define B2000000 2000000U
#endif
  switch(speed) {
    case 0:		baud = 0; break;
    case 9600:		baud = B9600; break;
    case 38400:		baud = B38400; break;
    case 57600:		baud = B57600; break;
    case 115200:	baud = B115200; break;
    case 256000:	baud = B256000; break;
    case 500000:	baud = B500000; break;
    case 2000000:	baud = B2000000; break;

    default: errno = EINVAL; return -1;
  }

  /* try to match a serial id first */
  path = usb_serial_to_tty(device);
  if (path) device = path;

  /* open non-blocking */
  fd = open(device, O_RDWR | O_NOCTTY | O_NONBLOCK);

  if (fd < 0) return fd;
  if (!isatty(fd)) {
    errno = ENOTTY;
    return -1;
  }

  /* configure line discipline */
  if (tcgetattr(fd, &t)) return -1;

  t.c_iflag = IGNBRK;
  t.c_oflag = 0;
  t.c_lflag = 0;
  t.c_cflag = CS8 | CREAD | CLOCAL;
  t.c_cc[VMIN] = 0;
  t.c_cc[VTIME] = 0;

  if (speed) {
    if (cfsetospeed(&t, baud)) return -1;
    if (cfsetispeed(&t, baud)) return -1;
  }

  if (tcsetattr(fd, TCSANOW, &t)) return -1;

  /* discard any pending data */
  tcflush(fd, TCIOFLUSH);

#ifdef HAVE_LOW_LATENCY_IOCTL
  /* Linux: enable low latency mode
   * see /sys/bus/usb-serial/devices/.../latency_timer
   */
  {
    struct serial_struct s;
    int e;

    if (ioctl(fd, TIOCGSERIAL, &s)) {
      e = errno;
      close(fd);
      errno = e;
      return -1;
    }

    if (!(s.flags & ASYNC_LOW_LATENCY)) {
      s.flags |= ASYNC_LOW_LATENCY;
      if (ioctl(fd, TIOCSSERIAL, &s)) {
        e = errno;
        close(fd);
        errno = e;
        return -1;
      }
    }
  }
#endif

  if (ftdi_sio_check_latency(fd)) {
    errno = EIO;
    return -1;
  }

  return fd;
}


/* --- usb_serial_to_tty --------------------------------------------------- */

/* Return a tty device matching the "serial" string */

static const char *
usb_serial_to_tty(const char *serial)
{
#ifdef __linux__
  struct udev *udev;
  struct udev_enumerate *scan = NULL;
  struct udev_list_entry *ttys, *tty;
  struct udev_device *dev = NULL, *usb;
  const char *path = NULL;

  udev = udev_new();
  if (!udev) return NULL;

  /* iterate over tty devices */
  scan = udev_enumerate_new(udev);
  if (udev_enumerate_add_match_subsystem(scan, "tty")) goto done;
  if (udev_enumerate_scan_devices(scan)) goto done;

  ttys = udev_enumerate_get_list_entry(scan);
  udev_list_entry_foreach(tty, ttys) {
    const char *sysfs, *userial;

    /* get sysfs entry for the device and create a corresponding udev_device */
    if (dev) udev_device_unref(dev);
    sysfs = udev_list_entry_get_name(tty);
    dev = udev_device_new_from_syspath(udev, sysfs);

    /* get the USB device, it any */
    usb = udev_device_get_parent_with_subsystem_devtype(
      dev, "usb", "usb_device");
    if (!usb) continue;

    userial = udev_device_get_sysattr_value(usb, "serial");
    if (!userial || strcmp(userial, serial)) continue;

    /* got a match, return the tty path */
    path = strdup(udev_device_get_devnode(dev)); /* this will leak ... */
    break;
  }
  if (dev) udev_device_unref(dev);

done:
  if (scan) udev_enumerate_unref(scan);
  if (udev) udev_unref(udev);
  return path;

#else
  (void)serial; /* -Wunused-parameter */

  return NULL; /* if needed, implement this for other OSes */
#endif
}


/* --- ftdi_sio_check_latency ---------------------------------------------- */

/* Check the latency value for ftdi_sio driver */

static int
ftdi_sio_check_latency(int fd)
{
#ifdef __linux__
  struct udev *udev;
  struct udev_device *dev, *parent;
  const char *attr;
  struct stat sb;
  int s = 0;

  /* get devno */
  if (fstat(fd, &sb)) return errno;

  /* get udev_device */
  udev = udev_new();
  if (!udev) {
    warnx("cannot use udev");
    return 1;
  }

  dev = udev_device_new_from_devnum(udev, 'c', sb.st_rdev);
  if (!dev) goto done;

  /* check if it is an ftdi_sio */
  for(parent = dev; parent; parent = udev_device_get_parent(parent)) {
    attr = udev_device_get_sysattr_value(parent, "driver");
    if (attr && !strcmp(attr, "ftdi_sio")) break;
  }
  if (!parent) goto done;

  /* access latency attribute */
  attr = udev_device_get_sysattr_value(parent, "latency_timer");
  if (!attr) {
    warnx("cannot access latency_timer");
    s = 1;
    goto done;
  }

  if (atoi(attr) > 1) {
    warnx("FTDI latency timer too high: %sms, should be 1ms", attr);
    s = 1;
  }

done:
  if (dev) udev_device_unref(dev);
  udev_unref(udev);
  return s;

#else
  (void)fd; /* -Wunused-parameter */

  return 0; /* if needed, implement this for other OSes */
#endif
}


/* --- mk_wait_msg --------------------------------------------------------- */

int
mk_wait_msg(const struct mk_channel_s *channel)
{
  //printf("mk_wait_msg()\n");
  struct pollfd pfd;
  int s;

  pfd.fd = channel->fd;
  pfd.events = POLLIN;//Data other than high-priority data may be read without blocking.

  s = poll(&pfd, 1, 500/*ms*/);//return the number of pollfd structures that have selected events

  if (pfd.revents & POLLHUP) {// POLLHUP : Device has been disconnected (revents only).
    close(pfd.fd);

    /* cheating with const. Oh well... */
    ((struct mk_channel_s *)channel)->fd = -1;
    warnx("disconnected from %s", channel->path);
  }

  return s;//if s>0 => an invent has occured
}


/* --- mk_recv_msg --------------------------------------------------------- */

/* returns: 0: timeout/incomplete, -1: error, 1: complete msg */

int
mk_recv_msg(struct mk_channel_s *chan, bool block)
{
  //printf("mk_recv_msg\n");
  struct iovec iov[2];//structure definie dans uio.h => ici on definit 2 tampons
  ssize_t s;
  uint8_t c;

  if (chan->fd < 0) return -1;

  do {
    /* feed the ring  buffer */
    iov[0].iov_base = chan->buf + chan->w;//addr de debut des données à transférer
    iov[1].iov_base = chan->buf;

    if (chan->r > chan->w) {//r et w sont des uint_8
      iov[0].iov_len = chan->r - chan->w - 1;//nbre d'octets à transférer => (pourquoi le -1)
      iov[1].iov_len = 0;
    } else if (chan->r > 0) {
      iov[0].iov_len = sizeof(chan->buf) - chan->w;
      iov[1].iov_len = chan->r - 1;
    } else {
      iov[0].iov_len = sizeof(chan->buf) - chan->w - 1;
      iov[1].iov_len = 0;
    }

    if (iov[0].iov_len || iov[1].iov_len) {
      do {
        s = readv(chan->fd, iov, 2);//recuperation de 2 octets
      } while(s < 0 && errno == EINTR);

      if (s < 0)
        return -1;
      else if (s == 0 && chan->start && block) {
        struct pollfd fd = { .fd = chan->fd, .events = POLLIN };

        s = poll(&fd, 1, 500/*ms*/);
        if (fd.revents & POLLHUP) return -1;
      } else if (s == 0 && chan->r == chan->w)
        return 0;
      else
        chan->w = (chan->w + s) % sizeof(chan->buf);
    }

    while(chan->r != chan->w) {
      c = chan->buf[chan->r];
      chan->r = (chan->r + 1) % sizeof(chan->buf);// car buffer circulaire

      switch(c) {
        case '^':
        chan->start = true;
        chan->escape = false;
        chan->len = 0;
        break;

        case '$':
          if (!chan->start) break;
          chan->start = false;

          switch(chan->msg[0]) {
            case 'N': /* info messages */
              warnx("hardware info: %.*s", chan->len-1, &chan->msg[1]);
              break;
            case 'A': /* warning messages */
              warnx("hardware warning: %.*s", chan->len-1, &chan->msg[1]);
              break;
            case 'E': /* error messages */
              warnx("hardware error: %.*s", chan->len-1, &chan->msg[1]);
              break;

            default: return 1;
          }
          break;

        case '!':
          chan->start = false;
          break;

        case '\\':
          chan->escape = true;
          break;

        default:
          if (!chan->start) break;
          if (chan->len >= sizeof(chan->msg)) {
            chan->start = false; break;
          }

          if (chan->escape) {
            c = ~c; chan->escape = false;
          }
          chan->msg[chan->len++] = c;
          break;
      }
    }
  } while(1);

  return 0;
}




int
mk_recv_msg_v3(struct mk_channel_s *chan, bool block)
{
  //printf("mk_receive_msg_v3()\n");
  struct iovec iov[2];
  ssize_t s;
  uint8_t c;

  if (chan->fd < 0) return -1;

  do {
    /* feed the ring  buffer */
    iov[0].iov_base = chan->buf + chan->w;
    iov[1].iov_base = chan->buf;

    if (chan->r > chan->w) {
      iov[0].iov_len = chan->r - chan->w - 1;
      iov[1].iov_len = 0;
    } else if (chan->r > 0) {
      iov[0].iov_len = sizeof(chan->buf) - chan->w;
      iov[1].iov_len = chan->r - 1;
    } else {
      iov[0].iov_len = sizeof(chan->buf) - chan->w - 1;
      iov[1].iov_len = 0;
    }

    if (iov[0].iov_len || iov[1].iov_len) {
      do {
        s = readv(chan->fd, iov, 2);
      } while(s < 0 && errno == EINTR);

      if (s < 0)
        return -1;
      else if (s == 0 && chan->start && block) {
        struct pollfd fd = { .fd = chan->fd, .events = POLLIN };

        s = poll(&fd, 1, 500/*ms*/);
        if (fd.revents & POLLHUP) return -1;
      } else if (s == 0 && chan->r == chan->w)
        return 0;
      else
        chan->w = (chan->w + s) % sizeof(chan->buf);
    }

    while(chan->r != chan->w) {
      c = chan->buf[chan->r];
      chan->r = (chan->r + 1) % sizeof(chan->buf);
      //printf("%c",c );//ecriture du carcatere recu
      switch(c) {
        case '#'://^
        chan->start = true;
        chan->escape = false;
        chan->len = 0;
        //printf("start=true\n");
        break;

        case '\r'://$
        //printf("detection fin de chaine\n");
          if (!chan->start) break;
          chan->start = false;

          switch(chan->msg[0]) {
            case 'N': /* info messages */
              warnx("hardware info: %.*s", chan->len-1, &chan->msg[1]);
              break;
            case 'A': /* warning messages */
              warnx("hardware warning: %.*s", chan->len-1, &chan->msg[1]);
              break;
            case 'E': /* error messages */
              warnx("hardware error: %.*s", chan->len-1, &chan->msg[1]);
              break;

            default: 
            //printf("return1 => on quitte mk_recv_msg_v3()\n");
              return 1;//seul moyen de quitetr le while 1
          }
          break;

        
        default:
          if (!chan->start) break;
          if (chan->len >= sizeof(chan->msg)) {
            chan->start = false; break;
          }

          if (chan->escape) {
            c = ~c; chan->escape = false;//pas besoin de faire ca dans la v3 
          }
          chan->msg[chan->len++] = c;//on ajoute le carcartère dans msg
          break;
      }
    }
  } while(1);//qu'est ce qui me fait sortir de cette boucle=> passer dans le default du switch case qui fait un return 1

  return 0;
}


/* --- mk_send_msg --------------------------------------------------------- */

static void	mk_encode(char x, char **buf);

int
mk_send_msg(const struct mk_channel_s *chan, const char *fmt, ...)
{
  va_list ap; //type for iterating arguments
  ssize_t s;  //signed size_t et size_t <=> unsigned int
  char buf[64], *w, *r;
  char c;

  if (chan->fd < 0) return -1;

  w = buf;

  va_start(ap, fmt); //Start iterating arguments with a va_list. argument1= va_list name argument2=last knonwn argument

  *w++ = '^';    //equivalent a *w ='^' et w=w+1   => on met le chapeau chinois en 1er element du tableau et on pinte a l'adresse d'après
  while((c = *fmt++)) { //tant qu'il y a des élèments dans la chaine de caractères passée en param (!='\0')
    

    /*voir a quoi ca sert*/
    while ((unsigned)(w - buf) > sizeof(buf)-8 /* 8 = worst case (4 bytes
                                                * escaped) */) {
      do 
      {
        s = write(chan->fd, buf, w - buf);//ecrit dans fd ce qu'il y a dans buf mais maxi la valeur (w-buf)  return the number of bytes written on success
      } while (s < 0 && errno == EINTR);   //pour etre sûr que le caractère a bien ete envoyé je crois

      if (s < 0) return -1;

      if (s > 0 && s < w - buf) memmove(buf, buf + s, w - buf - s);// voir https://koor.fr/C/cstring/memmove.wp  //memmove( destination, source, size );
      w -= s;//s est le nombre de caractere qui a ete envoye
    }//end (unsigned)(w - buf)



    switch(c) {
      case '%': {
        switch(*fmt++) {
          case '1':
            mk_encode(va_arg(ap, int/*promotion*/), &w);//retreive an argument param1=va_list name param2=type du parametre 
            break;

          case '2': {
            uint16_t x = va_arg(ap, int/*promotion*/);
            mk_encode((x >> 8) & 0xff, &w);
            mk_encode(x & 0xff, &w);
            break;
          }
          case '@': {
            uint16_t *x = va_arg(ap, uint16_t *);
            size_t l = va_arg(ap, size_t);
            while (l--) {
              mk_encode((*x >> 8) & 0xff, &w);
              mk_encode(*x & 0xff, &w);
              x++;
            }
            break;
          }

          case '4': {
            uint32_t x = va_arg(ap, uint32_t);
            mk_encode((x >> 24) & 0xff, &w);
            mk_encode((x >> 16) & 0xff, &w);
            mk_encode((x >> 8) & 0xff, &w);
            mk_encode(x & 0xff, &w);
            break;
          }
        }
        break;
      }

      default:
        mk_encode(c, &w);//encode ajoute le caractere dans buf[64] et traite les cas particulier
    }
  }//en while(chaine de caractere)
  


  *w++ = '$';//dernier caractere encodé
  va_end(ap);//Free a va_list


  r = buf;
  while (w > r) {
    do {
      s = write(chan->fd, r, w - r);
    } while (s < 0 && errno == EINTR);
    if (s < 0) return -1;

    r += s;
  }

  return 0;
}


static void
mk_encode_64(uint16_t *x, char **buf,size_t l);

static void 
AddCRC(unsigned int wieviele,char **buf,char *buff);

static void
mk_encode_v3(char x, char **buf);

int
mk_send_msg_v3(const struct mk_channel_s *chan, const char *fmt, ...)
{
  va_list ap; //type for iterating arguments
  ssize_t s;  //signed size_t et size_t <=> unsigned int
  char buf[64], *w, *r;
  char c,a;

  if (chan->fd < 0) return -1;

  w = buf;

  va_start(ap, fmt); //Start iterating arguments with a va_list. argument1= va_list name argument2=last knonwn argument

  *w++ = '#';    //equivalent a *w ='^' et w=w+1   => on met le chapeau chinois en 1er element du tableau et on pinte a l'adresse d'après
  while((c = *fmt++)) { //tant qu'il y a des élèments dans la chaine de caractères passée en param (!='\0')
    

    /*voir a quoi ca sert*/
    while ((unsigned)(w - buf) > sizeof(buf)-8 /* 8 = worst case (4 bytes
                                                * escaped) */) {
      do 
      {
        s = write(chan->fd, buf, w - buf);//ecrit dans fd ce qu'il y a dans buf mais maxi la valeur (w-buf)  return the number of bytes written on success
      } while (s < 0 && errno == EINTR);   //pour etre sûr que le caractère a bien ete envoyé je crois

      if (s < 0) return -1;

      if (s > 0 && s < w - buf) memmove(buf, buf + s, w - buf - s);// voir https://koor.fr/C/cstring/memmove.wp  //memmove( destination, source, size );
      w -= s;//s est le nombre de caractere qui a ete envoye
    }//end (unsigned)(w - buf)



    switch(c) {
      case '%': {  //plus besoin de ca normalement avec le firmware de mikrokopter => juste default case + mk_encode_64()
        switch(*fmt++) {
          case '1':
            mk_encode_v3(va_arg(ap, int/*promotion*/), &w);//retreive an argument param1=va_list name param2=type du parametre 
            break;

          case '2': {
            uint16_t x = va_arg(ap, int/*promotion*/);
            mk_encode_v3((x >> 8) & 0xff, &w);
            mk_encode_v3(x & 0xff, &w);
            break;
          }
          case '@': {  //pour la v3 on a besoin d'encoder 18 valeur pour tomber sur le bon nombre d'octets
            uint16_t *x = va_arg(ap, uint16_t *);//tableau d'int16
            size_t l = va_arg(ap, size_t);
            mk_encode_64(x, &w,l);
            break;
          }

          case '4': {
            uint32_t x = va_arg(ap, uint32_t);
            mk_encode_v3((x >> 24) & 0xff, &w);
            mk_encode_v3((x >> 16) & 0xff, &w);
            mk_encode_v3((x >> 8) & 0xff, &w);
            mk_encode_v3(x & 0xff, &w);
            break;
          }
        }
        break;
      }

      default:
        mk_encode_v3(c, &w);//encode ajoute le caractere dans buf[64] et traite les cas particulier
    }
  }//en while(chaine de caractere)
  
  va_end(ap);//Free a va_list
  r = buf;
  //printf("w-r=%ld\n",w-r);
  AddCRC(w-r,&w,buf);//w-r est le nombre d'elementqu'il y a dans buf
  //printf("w-r=%ld\n",w-r);
  while (w > r) {
    do {
      s = write(chan->fd, r, w - r);//ecrit sur fd ce qu'il y a dans r mais maxi w-r

    } while (s < 0 && errno == EINTR);
    if (s < 0) return -1;
    //printf("%d\n",s );
    r += s;
  }

 // printf("chaine envoye = %s\n",buf);
 // printf("\n");
  return 0;
}


static void
mk_encode(char x, char **buf)
{
  switch (x) {
    case '^': case '$': case '\\': case '!':
      *(*buf)++ = '\\';
      x = ~x;
  }
  *(*buf)++ = x;
}

static void
mk_encode_v3(char x, char **buf)
{
  *(*buf)++ = x;
}

static void
mk_encode_64(uint16_t *x, char **buf,size_t l)
{
  unsigned int pt = 0;
  unsigned char a,b,c;
  unsigned char ptr = 0;
  int len = l;
  for (int i = 0; i < 6; ++i)  //il y a 16 moteurs max et 18 est le multiple de 3 serieur le plus proche (necssaire a l'encodage)
  {
    /* code */
    if(len) { a = *x++; len--;} else a = 0;
    if(len) { b = *x++; len--;} else b = 0;
    if(len) { c = *x++; len--;} else c = 0;
    //printf("a,b,c = %d %d %d\n",a,b,c);
    //printf("len=%d\n", len);
    *(*buf)++ = '=' + (a >> 2);
    *(*buf)++ = '=' + (((a & 0x03) << 4) | ((b & 0xf0) >> 4));
    *(*buf)++ = '=' + (((b & 0x0f) << 2) | ((c & 0xc0) >> 6));
    *(*buf)++ = '=' + ( c & 0x3f);
  }
}

void AddCRC(unsigned int wieviele,char **buf,char *buff)
{
 unsigned int tmpCRC = 0,i;
 for(i = 0; i < wieviele;i++)
  {
    tmpCRC += buff[i];
  }

   tmpCRC %= 4096;
   *(*buf)++ = '=' + tmpCRC / 64;
   *(*buf)++ = '=' + tmpCRC % 64;
   *(*buf)++ = '\r';
   //printf("CRC1 : %d =>%c \n",'=' + tmpCRC / 64,'=' + tmpCRC / 64);
   //printf("CRC2 : %d =>%c \n",'=' + tmpCRC % 64,'=' + tmpCRC % 64);
 }

