/*
 * Copyright (c) 2015-2021 LAAS/CNRS
 * All rights reservd.
 *
 * Redistribution and use  in source  and binary  forms,  with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of  source  code must retain the  above copyright
 *      notice and this list of conditions.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice and  this list of  conditions in the  documentation and/or
 *      other materials provided with the distribution.
 *
 *					Anthony Mallet on Fri Feb 13 2015
 */
#include "acrotorcraft.h"

#include <sys/time.h>

#include <assert.h>
#include <err.h>
#include <errno.h>
#include <float.h>
#include <fnmatch.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include "rotorcraft_c_types.h"
#include "codels.h"


double yaw_ref=0., min, max;
int first=0;
/* supported devices */
static const struct {
  const char *match;		/* match string */
  double rev;			/* minimum revision */
  double gres, ares, mres;	/* imu resolutions */
} rc_devices[] = {
  [RC_MKBL] = {
    .match = "%*cmkbl%lf", .rev = 1.8,
    .gres = 1/1000., .ares = 1/1000., .mres = 1e-8
  },

  [RC_MKFL] = {
    .match = "mkfl%lf", .rev = 1.8,
    .gres = 1/1000., .ares = 1/1000., .mres = 1e-8
  },

  [RC_FLYMU] = {
    .match = "flymu%lf", .rev = 1.8,
    .gres = 1/1000., .ares = 1/1000., .mres = 1e-8
  },

  [RC_CHIMERA] = {
    .match = "chimera%lf", .rev = 1.1,
    .gres = 1000. * M_PI/180 / 32768,
    .ares = 8 * 9.81 / 32768,
    .mres = 1e-8
  },
};


static void	mk_get_ts(uint8_t seq, struct timeval atv, double rate,
                        rotorcraft_ids_sensor_time_s_ts_s *timings,
                        or_time_ts *ts, double *lprate);


/* --- Task comm -------------------------------------------------------- */

/** Codel mk_comm_start of task comm.
 *
 * Triggered by rotorcraft_start.
 * Yields to rotorcraft_poll.
 * Throws rotorcraft_e_sys.
 */
genom_event
mk_comm_start(rotorcraft_conn_s **conn, const genom_context self)
{
  *conn = malloc(sizeof(**conn));
  if (!*conn) return mk_e_sys_error(NULL, self);

  (*conn)->chan.fd = -1;
  (*conn)->chan.r = (*conn)->chan.w = 0;
  (*conn)->device = RC_NONE;

  return rotorcraft_poll;
}


/** Codel mk_comm_poll of task comm.
 *
 * Triggered by rotorcraft_poll.
 * Yields to rotorcraft_nodata, rotorcraft_recv.
 * Throws rotorcraft_e_sys.
 */
genom_event
mk_comm_poll(const rotorcraft_conn_s *conn, const genom_context self)
{
  //printf("\nCOMM_POLL\n");
  int s;

  s = mk_wait_msg(&conn->chan);
  //printf("s_comm_poll= %d\n",s );
  if (s < 0) {
    if (errno != EINTR) return mk_e_sys_error(NULL, self);
    return rotorcraft_nodata;
  }
  else if (s == 0) return rotorcraft_nodata;//timeout

  return rotorcraft_recv;
}


/** Codel mk_comm_nodata of task comm.
 *
 * Triggered by rotorcraft_nodata.
 * Yields to rotorcraft_poll.
 * Throws rotorcraft_e_sys.
 */
genom_event
mk_comm_nodata(rotorcraft_conn_s **conn,
               rotorcraft_ids_sensor_time_s *sensor_time,
               const rotorcraft_imu *imu, const rotorcraft_mag *mag,
               rotorcraft_ids_battery_s *battery,
               const genom_context self)
{
  //printf("COMM_NODATA\n");
  or_pose_estimator_state *idata = imu->data(self);
  or_pose_estimator_state *mdata = mag->data(self);

  /* reset exported data in case of timeout */
  idata->avel._present = false;
  idata->avel._value.wx = idata->avel._value.wy = idata->avel._value.wz =
    nan("");
  idata->acc._present = false;
  idata->acc._value.ax = idata->acc._value.ay = idata->acc._value.az =
    nan("");

  mdata->att._present = false;
  mdata->att._value.qw =
    idata->att._value.qy =
    idata->att._value.qz =
    idata->att._value.qx = nan("");

  battery->level = 0.;

  if (mk_set_sensor_rate(&sensor_time->rate, *conn, NULL, sensor_time, self))
    mk_disconnect_start(conn, self);

  return rotorcraft_poll;
}


/** Codel mk_comm_recv of task comm.
 *
 * Triggered by rotorcraft_recv.
 * Yields to rotorcraft_poll, rotorcraft_recv.
 * Throws rotorcraft_e_sys.
 */
int count_D,count_C;
genom_event
mk_comm_recv(rotorcraft_conn_s **conn,
             const rotorcraft_ids_imu_calibration_s *imu_calibration,
             const rotorcraft_ids_imu_filter_s *imu_filter,
             rotorcraft_ids_sensor_time_s *sensor_time,
             const rotorcraft_imu *imu, const rotorcraft_mag *mag,
             rotorcraft_ids_rotor_data_s *rotor_data,
             rotorcraft_ids_battery_s *battery,
             const genom_context self)
{
  struct timeval tv;
  int more;
  size_t i;
  uint8_t *msg, len;
  int16_t v16;
  uint16_t u16;
  double vc;
  //printf("MK_COMM_RECV\n");
  more = 0;
  if (mk_recv_msg_v3(&(*conn)->chan, false) == 1) {
    //printf("le if(mk_recv_msg_v3==1) a été validé car le caractere de fin de chaine de message a ete trouve dans la fonction recv\n");
    more = 1;
    gettimeofday(&tv, NULL);

    msg = (*conn)->chan.msg;
    len = (*conn)->chan.len;
   // printf("msg = %s\n",(*conn)->chan.msg);
   // printf("len = %d\n",(*conn)->chan.len);
   
    msg+=1; // que pour la v3 car l'ID est la 3 eme lettre et pas la 2 #bt  #bD
    //printf("\nID=%c\n",*msg ); // le premier carcatere apres le # de depart
    switch(*msg++) { //on vient lire le premier element
      //ajout des Id du protocole de mikrokopter
      case 't':
        //answer after having send a speed message to mikrokopter flight-ctrl firmware
        break;

      case 'D':
        count_D+=1;
        printf("count_D = %d\n",count_D);
        //traitement de la chaine de cractères recu
        if(len==92)//car on a passé le # et le b => m*msg pointe sur le D
        {
          //printf("comm_recv D  : %lu,%lu\n",tv.tv_sec,tv.tv_usec);
          count_D++;
         //printf("%lu\n",count_D);
          //printf("\nmsg = %s\n strlen(msg)= %d\n",msg,strlen(msg));//strlen=90 => il rest a supprimer les 2 CRC bytes
          msg[strlen(msg)-2]='\0';//car je souhaite uniquement isoler les 88 bytes de donnée
          //printf("\nmsg = %s\n strlen(msg)= %d\n",msg,strlen(msg));

          signed int analog_int[100];
          unsigned char analog[100];
          int analog_index=0;
          unsigned char a,b,c,d;
          unsigned char x,y,z;
         
          for(int i=0;i<22;++i) // 22*4=88 caracteres
          {
          
           a = *msg++ - '=';
           b = *msg++ - '=';
           c = *msg++ - '=';
           d = *msg++ - '=';
           
           
           x = (a << 2) | (b >> 4);
           y = ((b & 0x0f) << 4) | (c >> 2);
           z = ((c & 0x03) << 6) | d;
           //printf("x,y,z = %d,%d,%d\n",x,y,z );
          
            analog[analog_index++] = x;
            analog[analog_index++] = y;
            analog[analog_index++] = z;

          }

          signed char char1;
          signed char char2;
          unsigned char char3;
          unsigned char char4;
          signed int val;
          unsigned int val2;
          int j=0;
          for(int i=0;i<33;++i)  // 66/2 car on prend 2 octets pour avoir 1 unsigned int
          {
            //écrit les données de la structure
            //printf("%d ",analog[i]);
            //on regroupe par 2 les octets puis on actualise un tableau de signed int convention  
            if(i>1 && i%2==0)
            {
              char1=analog[i];
              char2=analog[i+1];
              char3=analog[i];
              char4=analog[i+1];
              if(i==14||i==16||i==26||i==28||i==30||i==32)
              {
                //printf("char3 = %d   char4= %d\n",char3,char4);    
                val=char4<<8|char3;
                analog_int[j++]=val;
              }
              else
              {
                val=char2<<8|char3;
                analog_int[j++]=val;
              }
            }
          }
           // system("clear");
           // for(int k=0;k<16;++k)
           // {
           //   switch(k)
           //   {
           //     case 0:
           //     printf("AngleNIck : %i\n",analog_int[k]);
           //     break;
           //     case 1:
           //     analog_int[k]=-analog_int[k];
           //     printf("AngleRoll : %i\n",analog_int[k]);
           //     break;
           //     case 2:
           //     printf("AccNIck : %i\n",analog_int[k]);
           //     break;
           //     case 3:
           //     printf("AccRoll : %i\n",analog_int[k]);
           //     break;
           //     case 4:
           //     printf("YawGyro : %i\n",analog_int[k]);
           //     break;
           //     case 5:
           //     printf("Altitude[0.1m] : %i\n",analog_int[k]);
           //     break;
           //     case 6:
           //     printf("AccZ : %i\n",analog_int[k]);
           //     break;
           //     case 7:
           //     printf("Gaz : %i\n",analog_int[k]);
           //     break;
           //     case 8:
           //     printf("Compass value : %i\n",analog_int[k]);
           //     break;
           //     case 9:
           //     printf("Voltage[0.1V] : %i\n",analog_int[k]);
           //     break;
           //     case 10:
           //     printf("Receiver Level : %i\n",analog_int[k]);
           //     break;
           //     case 11:
           //     printf("Gyro compass : %i\n",analog_int[k]);
           //     break;
           //     case 12:
           //     printf("Motor1 : %i\n",analog_int[k]);
           //     break;
           //     case 13:
           //     printf("Motor2 : %i\n",analog_int[k]);
           //     break;
           //     case 14:
           //     printf("Motor3 : %i\n",analog_int[k]);
           //     break;
           //     case 15:
           //     printf("Motor4 : %i\n",analog_int[k]);
           //     break;
           //   }
           // }
           //affichage des accelerations apres conversion
           analog_int[2]=-analog_int[2]*16682/1024; 
           analog_int[3]=-analog_int[3]*16682/1024; 
           analog_int[6]=(analog_int[6]-512)*3;
           analog_int[6]=19620*analog_int[6]/1024;  //19620 lu dans eeprom de mkfl
           // printf("\nAccNick : %d \n",analog_int[2]);
           // printf("AccRoll : %d \n",analog_int[3]);
           // printf("Accz : %d \n",analog_int[6]);

           //envoi des informations sue le port de sortie IMU
           or_pose_estimator_state *idata = imu->data(self);
           idata->acc._value.ax = (double)analog_int[3]/1000.;
           idata->acc._value.ay = (double)analog_int[2]/1000.;
           idata->acc._value.az = (double)analog_int[6]/1000.;
           idata->acc._present = true;
           idata->acc_cov._value.cov[0] = 0.01 * 0.01;
    	   idata->acc_cov._value.cov[1] = 0.;
	   idata->acc_cov._value.cov[2] = 0.01 * 0.01;
	   idata->acc_cov._value.cov[3] = 0.;
	   idata->acc_cov._value.cov[4] = 0.;
	   idata->acc_cov._value.cov[5] = 0.05 * 0.05;
	   idata->acc_cov._present = true;


           idata->ts.sec=tv.tv_sec;
           idata->ts.nsec=tv.tv_usec*1000;
           //imu->write(self);
           //printf("written\n");
          
           
         }
        
        break;

      case 'I': /* IMU data */
        // if (len == 14) {
        //   or_pose_estimator_state *idata = imu->data(self);
        //   double v[3];//acceleration  asclae et abias obtenu avec calib
        //   uint8_t seq = *msg++;

        //   if (seq == sensor_time->imu.seq) break;

        //   /* accelerometer */
        //   mk_get_ts(
        //     seq, tv, sensor_time->rate.imu, &sensor_time->imu,
        //     &idata->ts, &sensor_time->measured_rate.imu);

        //   v16 = ((int16_t)(*msg++) << 8);
        //   v16 |= ((uint16_t)(*msg++) << 0);
        //   v[0] = v16 * rc_devices[(*conn)->device].ares
        //          + imu_calibration->abias[0];

        //   v16 = ((int16_t)(*msg++) << 8);
        //   v16 |= ((uint16_t)(*msg++) << 0);
        //   v[1] = v16 * rc_devices[(*conn)->device].ares
        //          + imu_calibration->abias[1];

        //   v16 = ((int16_t)(*msg++) << 8);
        //   v16 |= ((uint16_t)(*msg++) << 0);
        //   v[2] = v16 * rc_devices[(*conn)->device].ares
        //          + imu_calibration->abias[2];

        //   vc =
        //     imu_calibration->ascale[0] * v[0] +
        //     imu_calibration->ascale[1] * v[1] +
        //     imu_calibration->ascale[2] * v[2];
        //   if (!isnan(idata->acc._value.ax))
        //     idata->acc._value.ax +=
        //       imu_filter->aalpha[0] * (vc - idata->acc._value.ax);
        //   else
        //     idata->acc._value.ax = vc;

        //   vc =
        //     imu_calibration->ascale[3] * v[0] +
        //     imu_calibration->ascale[4] * v[1] +
        //     imu_calibration->ascale[5] * v[2];
        //   if (!isnan(idata->acc._value.ay))
        //     idata->acc._value.ay +=
        //       imu_filter->aalpha[1] * (vc - idata->acc._value.ay);
        //   else
        //     idata->acc._value.ay = vc;

        //   vc =
        //     imu_calibration->ascale[6] * v[0] +
        //     imu_calibration->ascale[7] * v[1] +
        //     imu_calibration->ascale[8] * v[2];
        //   if (!isnan(idata->acc._value.az))
        //     idata->acc._value.az +=
        //       imu_filter->aalpha[2] * (vc - idata->acc._value.az);
        //   else
        //     idata->acc._value.az = vc;

        //   /* gyroscope */
        //   v16 = ((int16_t)(*msg++) << 8);
        //   v16 |= ((uint16_t)(*msg++) << 0);
        //   v[0] = v16 * rc_devices[(*conn)->device].gres
        //          + imu_calibration->gbias[0];

        //   v16 = ((int16_t)(*msg++) << 8);
        //   v16 |= ((uint16_t)(*msg++) << 0);
        //   v[1] = v16 * rc_devices[(*conn)->device].gres
        //          + imu_calibration->gbias[1];

        //   v16 = ((int16_t)(*msg++) << 8);
        //   v16 |= ((uint16_t)(*msg++) << 0);
        //   v[2] = v16 * rc_devices[(*conn)->device].gres
        //          + imu_calibration->gbias[2];

        //   vc =
        //     imu_calibration->gscale[0] * v[0] +
        //     imu_calibration->gscale[1] * v[1] +
        //     imu_calibration->gscale[2] * v[2];
        //   if (!isnan(idata->avel._value.wx))
        //     idata->avel._value.wx +=
        //       imu_filter->galpha[0] * (vc - idata->avel._value.wx);
        //   else
        //     idata->avel._value.wx = vc;

        //   vc =
        //     imu_calibration->gscale[3] * v[0] +
        //     imu_calibration->gscale[4] * v[1] +
        //     imu_calibration->gscale[5] * v[2];
        //   if (!isnan(idata->avel._value.wy))
        //     idata->avel._value.wy +=
        //       imu_filter->galpha[1] * (vc - idata->avel._value.wy);
        //   else
        //     idata->avel._value.wy = vc;

        //   vc =
        //     imu_calibration->gscale[6] * v[0] +
        //     imu_calibration->gscale[7] * v[1] +
        //     imu_calibration->gscale[8] * v[2];
        //   if (!isnan(idata->avel._value.wz))
        //     idata->avel._value.wz +=
        //       imu_filter->galpha[2] * (vc - idata->avel._value.wz);
        //   else
        //     idata->avel._value.wz = vc;

        //   idata->avel._present = true;
        //   idata->acc._present = true;
        // } else
        //   warnx("bad IMU message");
        break;

      case 'C': /* magnetometer data */
        //printf("****************data3D_struct*********************\n");
     // printf("comm_recv C : %lu,%lu\n",tv.tv_sec,tv.tv_usec);
     count_C+=1;
     printf("count_C = %d\n",count_C);
     if(len==24) //15 encoded + 2CRC + bD
        {
          signed int analog_int[100];
          unsigned char analog[100];
          int analog_index=0;
          unsigned char a,b,c,d;
          unsigned char x,y,z;
         
          for(int i=0;i<5;++i) // 5*4=20 caracteres
          {
          
           a = *msg++ - '=';
           b = *msg++ - '=';
           c = *msg++ - '=';
           d = *msg++ - '=';
           
           
           x = (a << 2) | (b >> 4);
           y = ((b & 0x0f) << 4) | (c >> 2);
           z = ((c & 0x03) << 6) | d;
           //printf("x,y,z = %d,%d,%d\n",x,y,z );
          
            analog[analog_index++] = x;
            analog[analog_index++] = y;
            analog[analog_index++] = z;
          }

          
          signed char char1;
          signed char char2;
          unsigned char char3;
          unsigned char char4;
          signed int val;
          unsigned int val2;
          int j=0;
          for(int i=0;i<5;++i)  
          {
            //écrit les données de la structure
            //printf("%d ",analog[i]);
            //on regroupe par 2 les octets puis on actualise un tableau de signed int convention  
            if(i%2==0)
            {
              char1=analog[i];
              char2=analog[i+1];
              char3=analog[i];
              char4=analog[i+1];
              val=char2<<8|char3;
              analog_int[j++]=val;
            }
          }
          if(first==0)
          {
            first+=1;
            yaw_ref=(double)analog_int[2]/10.;
          }



          //system("clear");

          //printf("%f\n", yaw_ref);
          double roll,pitch,yaw;
          pitch=(double)analog_int[0]/10.;
          //printf("nick/pitch= %f\n",pitch);
          roll=-((double)analog_int[1]/10.);
          //printf("roll= %f\n",roll);
         
          double angle;
          double compass=analog_int[2]/10.;
          if (compass >= yaw_ref)
            angle = compass - yaw_ref;
          else 
            angle = 360 - yaw_ref + compass;
          //printf("angle [0;360] = %f\n",angle);
          if (angle < 180.)
            angle = -(angle);
          else
            angle = -(angle - 360.);
          //printf("angle [-180;180] = %f\n",angle);

          yaw=angle;

          pitch*=M_PI/180.;
          roll*=M_PI/180.;
          yaw*=M_PI/180.;

          double qx=sin(roll/2)*cos(pitch/2)*cos(yaw/2)-cos(roll/2)*sin(pitch/2)*sin(yaw/2);
          double qy=cos(roll/2)*sin(pitch/2)*cos(yaw/2)+sin(roll/2)*cos(pitch/2)*sin(yaw/2);
          double qz=cos(roll/2)*cos(pitch/2)*sin(yaw/2)-sin(roll/2)*sin(pitch/2)*cos(yaw/2);
          double qw=cos(roll/2)*cos(pitch/2)*cos(yaw/2)+sin(roll/2)*sin(pitch/2)*sin(yaw/2);

          //printf("quaternion = %f %f %f %f\n",qw, qx ,qy,qz);
          or_pose_estimator_state *idata = imu->data(self);
          idata->att._value.qw=qw;
          idata->att._value.qx=qx;
          idata->att._value.qy=qy;
          idata->att._value.qz=qz;
          idata->att_cov._value.cov[0] = 0,000025 * (1 - qw*qw);
          idata->att_cov._value.cov[1] = 0,000025 * -qw*qx;
          idata->att_cov._value.cov[2] = 0,000025 * (1 - qx*qx);
          idata->att_cov._value.cov[3] = 0,000025 * qw*qy;
          idata->att_cov._value.cov[4] = 0,000025 * -qx*qy;
          idata->att_cov._value.cov[5] = 0,000025 * (1 - qy*qy);
          idata->att_cov._value.cov[6] = 0,000025 * -qw*qz;
          idata->att_cov._value.cov[7] = 0,000025 * -qx*qz;
          idata->att_cov._value.cov[8] = 0,000025 * -qy*qz;
          idata->att_cov._value.cov[9] = 0,000025 * (1 - qz*qz);
          idata->att._present=true;
          idata->ts.sec=tv.tv_sec;
          idata->ts.nsec=tv.tv_usec*1000;
        }

       
         

        
        //system("clear");
        ////code for mkfl firmware
        // if (len == 8) {
        //   or_pose_estimator_state *mdata = mag->data(self);
        //   double v[3];
        //   uint8_t seq = *msg++;

        //   if (seq == sensor_time->mag.seq) break;

        //   mk_get_ts(
        //     seq, tv, sensor_time->rate.mag, &sensor_time->mag,
        //     &mdata->ts, &sensor_time->measured_rate.mag);

        //   v16 = ((int16_t)(*msg++) << 8);
        //   v16 |= ((uint16_t)(*msg++) << 0);
        //   v[0] = v16 * rc_devices[(*conn)->device].mres
        //          + imu_calibration->mbias[0];

        //   v16 = ((int16_t)(*msg++) << 8);
        //   v16 |= ((uint16_t)(*msg++) << 0);
        //   v[1] = v16 * rc_devices[(*conn)->device].mres
        //          + imu_calibration->mbias[1];

        //   v16 = ((int16_t)(*msg++) << 8);
        //   v16 |= ((uint16_t)(*msg++) << 0);
        //   v[2] = v16 * rc_devices[(*conn)->device].mres
        //          + imu_calibration->mbias[2];

        //   mdata->att._value.qw = nan("");

        //   vc =
        //     imu_calibration->mscale[0] * v[0] +
        //     imu_calibration->mscale[1] * v[1] +
        //     imu_calibration->mscale[2] * v[2];
        //   if (!isnan(mdata->att._value.qx))
        //     mdata->att._value.qx +=
        //       imu_filter->malpha[0] * (vc - mdata->att._value.qx);
        //   else
        //     mdata->att._value.qx = vc;

        //   vc =
        //     imu_calibration->mscale[3] * v[0] +
        //     imu_calibration->mscale[4] * v[1] +
        //     imu_calibration->mscale[5] * v[2];
        //   if (!isnan(mdata->att._value.qy))
        //     mdata->att._value.qy +=
        //       imu_filter->malpha[1] * (vc - mdata->att._value.qy);
        //   else
        //     mdata->att._value.qy = vc;

        //   vc =
        //     imu_calibration->mscale[6] * v[0] +
        //     imu_calibration->mscale[7] * v[1] +
        //     imu_calibration->mscale[8] * v[2];
        //   if (!isnan(mdata->att._value.qz))
        //     mdata->att._value.qz +=
        //       imu_filter->malpha[2] * (vc - mdata->att._value.qz);
        //   else
        //     mdata->att._value.qz = vc;

        //   mdata->att._present = true;
        // } else
        //   warnx("bad magnetometer message");
        break;

      case 'M': /* motor data */
        if (len == 9) {
          uint8_t seq = *msg++;
          uint8_t state = *msg++;
          uint8_t id = state & 0xf;

          if (id < 1 || id > or_rotorcraft_max_rotors) break;
          id--;
          if (seq == sensor_time->motor[id].seq) break;

          if (!rotor_data->state[id].ts.sec && rotor_data->state[id].disabled)
            rotor_data->state[id].disabled = 0;

          mk_get_ts(
            seq, tv, sensor_time->rate.motor, &sensor_time->motor[id],
            &rotor_data->state[id].ts, &sensor_time->measured_rate.motor);

          rotor_data->state[id].emerg = !!(state & 0x80);
          rotor_data->state[id].spinning = !!(state & 0x20);
          rotor_data->state[id].starting = !!(state & 0x10);

          v16 = ((int16_t)(*msg++) << 8);
          v16 |= ((uint16_t)(*msg++) << 0);
          if (rotor_data->state[id].spinning)
            rotor_data->state[id].velocity = v16 ? 1e6/2/v16 : 0.;
          else
            rotor_data->state[id].velocity = 0.;

          v16 = ((int16_t)(*msg++) << 8);
          v16 |= ((uint16_t)(*msg++) << 0);
          rotor_data->state[id].throttle = v16 * 100./1023.;

          u16 = ((uint16_t)(*msg++) << 8);
          u16 |= ((uint16_t)(*msg++) << 0);
          rotor_data->state[id].consumption = u16 / 1e3;
        } else
          warnx("bad motor data message");
        break;

      case 'B': /* battery data */
        if (len == 4) {
          uint8_t seq  __attribute__((unused)) = *msg++;
          double p;

          u16 = ((uint16_t)(*msg++) << 8);
          u16 |= ((uint16_t)(*msg++) << 0);
          battery->level = u16/1000.;

          p = 100. *
              (battery->level - battery->min)/(battery->max - battery->min);
          for(i = 0; i < or_rotorcraft_max_rotors; i++)
            rotor_data->state[i].energy_level = p;
        } else
          warnx("bad battery message");
        break;

      case 'T': /* clock rate */
        // if (len == 3) {
        //   uint8_t id = *msg++;

        //   if (id < 1 || id > or_rotorcraft_max_rotors) break;
        //   id--;
        //   rotor_data->clkrate[id] = *msg;
        // } else
        //   warnx("bad clock rate message");
        break;

      case '?': /* ignored messages */
        break;

      default:
        warnx("received unknown message");
    }
  }

  return more ? rotorcraft_recv : rotorcraft_poll;
}


/** Codel mk_comm_stop of task comm.
 *
 * Triggered by rotorcraft_stop.
 * Yields to rotorcraft_ether.
 * Throws rotorcraft_e_sys.
 */
genom_event
mk_comm_stop(rotorcraft_conn_s **conn, const genom_context self)
{
  if (!*conn) return rotorcraft_ether;

  mk_send_msg(&(*conn)->chan, "x");
  mk_set_sensor_rate(
    &(struct rotorcraft_ids_sensor_time_s_rate_s){ 0 }, *conn,
    NULL, NULL, self);

  return rotorcraft_ether;
}


/* --- Activity connect ------------------------------------------------- */

/** Codel mk_connect_start of activity connect.
 *
 * Triggered by rotorcraft_start.
 * Yields to rotorcraft_ether.
 * Throws rotorcraft_e_sys, rotorcraft_e_baddev.
 */
genom_event
mk_connect_start(const char serial[64], uint32_t baud,
                 rotorcraft_conn_s **conn,
                 rotorcraft_ids_sensor_time_s *sensor_time,
                 const genom_context self)
{
  //printf("activity_connect\n");
  double rev;
  size_t c;
  int s;

  if ((*conn)->chan.fd >= 0) {
    close((*conn)->chan.fd);
    warnx("disconnected from %s", (*conn)->chan.path);
  }

  //printf("1: %d\n", baud);

  /* open tty */
  (*conn)->chan.fd = mk_open_tty(serial, baud);
  if ((*conn)->chan.fd < 0) return mk_e_sys_error(serial, self);

  //printf("2\n");

  (*conn)->chan.r = (*conn)->chan.w = 0;
  (*conn)->chan.start = (*conn)->chan.escape = false;

  // /* check endpoint */
  // while (mk_recv_msg(&(*conn)->chan, true) == 1); /* flush buffer */
  // do {
  //   c = 0;
  //   do {
  //     if (mk_send_msg(&(*conn)->chan, "?")) /* ask for id */
  //       return mk_e_sys_error(serial, self);

  //     s = mk_wait_msg(&(*conn)->chan);
  //     if (s < 0 && errno != EINTR) return mk_e_sys_error(NULL, self);
  //     if (s > 0) break;
  //   } while(c++ < 3);
  //   if (c > 3) {
  //     errno = ETIMEDOUT;
  //     return mk_e_sys_error(NULL, self);
  //   }

  //   s = mk_recv_msg(&(*conn)->chan, true);
  // } while(s == 1 && (*conn)->chan.msg[0] != '?');
  // if (s != 1) {
  //   errno = ENOMSG;
  //   return mk_e_sys_error(NULL, self);
  // }

  /* match device */
  //printf("3\n");
  (*conn)->chan.msg[(*conn)->chan.len] = 0;
  (*conn)->device = RC_NONE;
  for (c = 0; c < sizeof(rc_devices)/sizeof(rc_devices[0]); c++) {
    if (!rc_devices[c].match) continue;

    if (sscanf((char *)&(*conn)->chan.msg[1], rc_devices[c].match, &rev) != 1)
      continue;
    if (rev < rc_devices[c].rev) {
      rotorcraft_e_baddev_detail d;
      snprintf(d.dev, sizeof(d.dev), "hardware device version `%g' too old, "
               "version `%g' or newer is required", rev, rc_devices[c].rev);
      close((*conn)->chan.fd);
      (*conn)->chan.fd = -1;
      return rotorcraft_e_baddev(&d, self);
    }

    (*conn)->device = c;
    break;
  }
  //printf("4\n");

  // if ((*conn)->device == RC_NONE) {
  //   rotorcraft_e_baddev_detail d;
  //   snprintf(d.dev, sizeof(d.dev), "unsupported hardware device `%s'",
  //            &(*conn)->chan.msg[1]);
  //   close((*conn)->chan.fd);
  //   (*conn)->chan.fd = -1;
  //   return rotorcraft_e_baddev(&d, self);
  // }

  snprintf((*conn)->chan.path, sizeof((*conn)->chan.path), "%s", serial);
  warnx("connected to %s, %s", &(*conn)->chan.msg[1], (*conn)->chan.path);

  /* configure data streaming */
  mk_set_sensor_rate(&sensor_time->rate, *conn, NULL, sensor_time, self);

  //pour la v3
  mk_send_msg_v3(&((*conn)->chan), "cu=B^[");//le # est ajouté dans la fonction avec crc bytes et \r
  printf("connexion message sent for v3.0\n");

  return rotorcraft_ether;
}


/* --- Activity disconnect ---------------------------------------------- */

/** Codel mk_disconnect_start of activity disconnect.
 *
 * Triggered by rotorcraft_start.
 * Yields to rotorcraft_ether.
 * Throws rotorcraft_e_sys.
 */
genom_event
mk_disconnect_start(rotorcraft_conn_s **conn,
                    const genom_context self)
{
  mk_send_msg(&(*conn)->chan, "x");
  mk_set_sensor_rate(
    &(struct rotorcraft_ids_sensor_time_s_rate_s){
      .imu = 0, .motor = 0, .battery = 0
        }, *conn, NULL, NULL, self);

  if ((*conn)->chan.fd >= 0) {
    close((*conn)->chan.fd);
    warnx("disconnected from %s", (*conn)->chan.path);
  }
  (*conn)->chan.fd = -1;

  return rotorcraft_ether;
}


/* --- Activity monitor ------------------------------------------------- */

/** Codel mk_monitor_check of activity monitor.
 *
 * Triggered by rotorcraft_start, rotorcraft_sleep.
 * Yields to rotorcraft_pause_sleep, rotorcraft_ether.
 * Throws rotorcraft_e_sys.
 */
genom_event
mk_monitor_check(const rotorcraft_conn_s *conn,
                 const genom_context self)
{
  (void)self;

  if (conn->chan.fd >= 0) return rotorcraft_pause_sleep;

  return rotorcraft_ether;
}


/* --- mk_get_ts ----------------------------------------------------------- */

/** Implements Olson, Edwin. "A passive solution to the sensor synchronization
 * problem." International conference on Intelligent Robots and Systems (IROS),
 * 2010 IEEE/RSJ */
static void
mk_get_ts(uint8_t seq, struct timeval atv, double rate,
          rotorcraft_ids_sensor_time_s_ts_s *timings, or_time_ts *ts,
          double *lprate)
{
  static const uint32_t tsshift = 1000000000;

  double ats, df;
  uint8_t ds;

  /* arrival timestamp - offset for better floating point precision */
  ats = (atv.tv_sec - tsshift) + atv.tv_usec * 1e-6;

  /* update estimated rate */
  df = 1. / (ats - timings->last);

  if (df > timings->rmed)
    timings->rerr = (3 * timings->rerr + 1.) / 4.;
  else
    timings->rerr = (3 * timings->rerr - 1.) / 4.;

  if (fabs(timings->rerr) > 0.75)
    timings->rgain *= 2;
  else
    timings->rgain /= 2;
  if (timings->rgain < 0.01) timings->rgain = 0.01;

  if (df > timings->rmed)
    timings->rmed += timings->rgain;
  else
    timings->rmed -= timings->rgain;

  *lprate += 0.1 * (timings->rmed - *lprate);

  /* delta samples */
  ds = seq - timings->seq;
  if (ds > 16)
    /* if too many samples were lost, we might have missed more than 255
     * samples: reset the offset */
    timings->offset = -DBL_MAX;
  else if (rate > 0.1)
    /* consider a 0.1% clock drift on the sender side (for rates >0.1Hz) */
    timings->offset -= 0.001 * ds / rate;
  else
    timings->offset = 0.;

  /* update remote timestamp */
  timings->last = ats;
  timings->seq = seq;
  if (rate > 0.1)
    timings->ts += ds / rate;
  else
    /* for tiny rates, just use arrival timestamp */
    timings->ts = ats;

  /* update offset */
  if (timings->ts - ats > timings->offset)
    timings->offset = timings->ts - ats;

  /* local timestamp - reset offset if it diverged more than 5ms from realtime,
   * maybe the sensor is not sending at the specified rate */
  if (ats - (timings->ts - timings->offset) > 0.005)
    timings->offset = timings->ts - ats;
  else
    ats = timings->ts - timings->offset;

  /* update timestamp */
  ts->sec = floor(ats);
  ts->nsec = (ats - ts->sec) * 1e9;
  ts->sec += tsshift;
}
